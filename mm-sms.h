#ifndef __MM_H_INCLUDE__
#define __MM_H_INCLUDE__

typedef struct {
  PurpleAccount    *account;

  MMManager        *mm;
  MMObject         *object;
  MMModem          *modem;
  MMSim            *sim;
  MMModemMessaging *modem_messaging;

  GPtrArray        *sms_arr;
  GPtrArray        *device_arr;
  gboolean          modem_available;
  gboolean          manager_available;

  gchar            *sms_id;
  guint             sms_validity;
  gboolean          sms_delete_sent;
  gboolean          sms_delete_received;
  gboolean          sms_delivery_report;

  guint             lock_type;
  guint             mm_watch_id;
} pur_mm_data_t;

pur_mm_data_t *pur_mm_get_data (void);


enum {
  PUR_MM_STATE_NO_MANAGER,
  PUR_MM_STATE_MANAGER_FOUND,
  PUR_MM_STATE_NO_MODEM,
  PUR_MM_STATE_MODEM_FOUND,
  PUR_MM_STATE_NO_MESSAGING_MODEM,
  PUR_MM_STATE_MODEM_DISABLED,
  PUR_MM_STATE_MODEM_UNLOCK_ERROR,
  PUR_MM_STATE_READY
} e_purple_connection;


 enum {
  SMS_VADILITY_SMC_DEFAULT,
  SMS_VADILITY_24_HOURS,
  SMS_VADILITY_7_DAYS
} e_sms_vadility;


enum
{
  SMS_RECEIPT_NONE     = -1,
  SMS_RECEIPT_MM_ACKN  =  0,
  SMS_RECEIPT_SMC_ACKN,
  SMS_RECEIPT_SMC_NACK
} e_sms_receipt_states;


#endif
