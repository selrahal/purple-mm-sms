
CC = gcc
LIBS = purple mm-glib
PKG_CONFIG = pkg-config

CFLAGS += $(shell $(PKG_CONFIG) --cflags $(LIBS))
CFLAGS += -fPIC -DPIC
LDLIBS += $(shell $(PKG_CONFIG) --libs $(LIBS))

DIR_PERM  = 0755
FILE_PERM = 0644

PLUGIN_DIR_PURPLE	   = $(shell $(PKG_CONFIG) --variable=plugindir purple)
DATA_ROOT_DIR_PURPLE = $(shell $(PKG_CONFIG) --variable=datarootdir purple)

TARGET = mm-sms.so

COMPILE.c = $(CC) $(CFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c

CFLAGS += -Wall -g -O0 #-Werror
CFLAGS += -DPURPLE_PLUGINS

OBJECTS = mm-sms.o

all: $(TARGET)

clean:
	rm -f $(OBJECTS) $(OBJECTS:.o=.d) $(TARGET)

install:
	mkdir   -p $(DESTDIR)$(PLUGIN_DIR_PURPLE)
	install -m $(FILE_PERM) $(TARGET) $(DESTDIR)$(PLUGIN_DIR_PURPLE)
	for i in 16 22 48; do \
	    mkdir   -p $(DESTDIR)$(DATA_ROOT_DIR_PURPLE)/pixmaps/pidgin/protocols/$$i; \
	    install -m $(FILE_PERM) icons/mm-sms-$${i}px.png $(DESTDIR)$(DATA_ROOT_DIR_PURPLE)/pixmaps/pidgin/protocols/$$i/mm-sms.png; \
	done

$(TARGET): $(OBJECTS)
	$(LINK.o) -shared $^ $(LOADLIBES) $(LDLIBS) -o $@

%.o: %.c
	$(COMPILE.c) $(OUTPUT_OPTION) $<

-include $(OBJECTS:.o)
